# README 
# arduino-expression-pedal

My goal for this project was to convert an analog expression pedal, in my case the M-Audio EX-P (http://www.m-audio.com/products/view/ex-p), into a MIDI-enabled controller

### The code does the following ###
* reading the potentiometer value
* applies smoothing (avarageing)
* maps the potentiometer value to the 0-255 that MIDI supports
* sends a MIDI message if the value has changed

### Used libraries / components ###
* Arduino MIDIUSB library (https://www.arduino.cc/en/Reference/MIDIUSB)
* Drawing heavily from this [Arduino example](https://www.arduino.cc/en/Tutorial/Smoothing)
* Inspired by [this project](https://www.codeproject.com/Articles/38203/Arduino-Based-MIDI-Expression-Pedal)

### Requirements / needed modifications ###
* a microcontroller with native USB (atmega32u4-based or ARM)
* soldering the connectors of the pedal potentiometer in the pedal to the arduino
* installing a usb connector in the pedal case

### Version history ###
* Ver: 1.0 - Initial version. Date: 2018-02-14

### Workflow ###
* Make changes on local branch and commit
* Change to master and pull
* Merge
* Push master, delete branch

### Contact ###

* Johan Klinge
* johan.klinge@gmail.com

