/*
 * For MIDI expression controller 
 * Read pot value, smooth by averaging, map to 0-255, sompare to
 * two last mapped values, send as Midi CC if different from both
 * 
 * Set amount of smoothing by changing const numReadings
 * 
 * Created: 2018-02-01
 * Author: Johan Klinge
 */ 

#include <Arduino.h>
#include "MIDIUSB.h"

// CONSTANTS
const int MIDI_CHANNEL = 4;
const int MIDI_CONTROL = 110;
const int DEBUG = 0;            // turn debug printing to serial console on or off
const int numReadings = 6;      // number of readings used to calculate average

//VARIABLES
int readings[numReadings];      // stored readings from the analog input
int mappedValue[2] = {0, 0};    // store two last mapped values
int readIndex = 0;              // the index of the current reading
int mapIndex = 0;               // index for the mapped values
int total = 0;                  // the running total
int average = 0;                // the average
int inputPin = A0;              // input pin for the potentiometer

//FUNCTION PROTOTYPES
void sendControlChange(byte channel, byte control, byte value); 
void print_array(int a[], int num_elements);

void setup() {
  Serial.begin(115200);
  // initialize all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
}

void loop() {
  // subtract the last reading:
  total = total - readings[readIndex];
  
  // read from the sensor:
  readings[readIndex] = analogRead(inputPin);
  
  // add the reading to the total and calculate new average:
  total = total + readings[readIndex];
  average = total/numReadings;
  
  //Print contents of readings array
  if(DEBUG) {
    Serial.print(readIndex);
    Serial.println(" ----------------------------");
    print_array(readings, numReadings);
  }

  //map average sensor value to 0-255 range
  int newMappedValue = map(average, 1023, 0, 255, 0);
  
  if (DEBUG) {
    Serial.print("Mapped val: ");
    Serial.print(newMappedValue);
    Serial.print(". Senaste: ");
    print_array(mappedValue, 2);
  }

  //check if same as the two last mapped values
  if( mappedValue[1] != newMappedValue && mappedValue[0] != newMappedValue ) {
    if(DEBUG) {
      Serial.print(">>MIDI ut. Värde: ");
      Serial.println(newMappedValue);
    }
    sendControlChange(MIDI_CHANNEL, MIDI_CONTROL, newMappedValue); 
    MidiUSB.flush();
  }
  // advance to the next position in the readings array:
  readIndex = readIndex + 1;
  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
  //save mapped value
  mappedValue[mapIndex] = newMappedValue;
  //toggle mapIndex using ternary op
  (mapIndex) ? (mapIndex=0) : (mapIndex=1);
  delay(5);
}

// UTIL FUNCTIONS 

// First parameter is the event type (0x0B = control change).
// Second parameter is the event type, combined with the channel.
// Third parameter is the control number number (0-119).
// Fourth parameter is the control value (0-127).
void sendControlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void print_array(int a[], int num_elements)
{
   int i;
   Serial.print("[");
   for(i=0; i<num_elements-1; i++)
   {
	   Serial.print(a[i]);
     Serial.print(",");
   }
   Serial.print(a[num_elements-1]);
   Serial.println("]");
}